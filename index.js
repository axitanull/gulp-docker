const npexecSync    = require('child_process').execSync;
const through       = require('through2');
const data          = require('gulp-data');
const gutil         = require('gulp-util');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder       = new StringDecoder('utf8');
require('dirutils');

const utils = {
    getFilterString: function (filters = []) {
        let filterArgs = filters.map((filter) => `--filter="${filter}"`);
        return filterArgs.join(" ");
    },
    getIdStringFromChunk: function (chunk) {
        let nameId = chunk.data.ids.split("\n");
        if (nameId.constructor === Array) {
            nameId = nameId.join(" ");
        }
        return nameId;
    }
};

const npexecSyncOutput = function (cmd, options = {}) {
    console.log(`> ${cmd}`);
    let output = decoder.write(npexecSync(cmd, options));
    output     = output.trim();
    return output;
};

const docker = {
    compose: function ({workpath='.', composeFile='docker-compose.yml'} = {}) {
        return through.obj(function (chunk, enc, callback) {
            npexecSync(`docker-compose -f ${composeFile} up -d --no-recreate`, {cwd: workpath, stdio: 'inherit'});
            this.push(chunk);
            callback();
        });
    },
    build: function ({dockerFile='', imageName='', workpath='.', filters} = {}) {
        return through.obj(function (chunk, enc, callback) {
            gutil.log(`Build image for: ${dockerFile}`);

            //- Skip build if image with defined filters exists
            if (filters) {
                let imageIds = npexecSyncOutput(`echo $(docker images ${utils.getFilterString(filters)} -q)`);
                if (imageIds) {
                    gutil.log(gutil.colors.magenta('Image exists, SKIP build.'));
                    this.push(chunk);
                    callback();
                    return;
                } else {
                    gutil.log('Image NOT exists, proceeding with build.');
                }
            }

            process.pushdir(workpath);
            npexecSync(`docker build ${imageName ? `-t ${imageName}` : ""} ${dockerFile ? `-f ${dockerFile}` : ""} .`, {stdio: 'inherit'});
            process.popdir();

            this.push(chunk);
            callback();
        });
    },
    cp: function ({src, dest} = {}) {
        gutil.log(`Copy: ${src} -> ${dest}`);
        npexecSync(`docker cp ${src} ${dest}`, {stdio: 'inherit'});
    },
    exec: function ({container, command}) {
        npexecSync(`docker exec ${container} ${command}`);
    },
    images: function ({filters}={}) {
        return data(function (file) {
            file.ids = npexecSyncOutput(`docker images -q ${utils.getFilterString(filters)}`);
            return file;
        });
    },
    ps: function ({filters}={}) {
        return data(function (file) {
            file.ids = npexecSyncOutput(`docker ps -a -q ${utils.getFilterString(filters)}`);
            return file;
        });
    },
    restart: function (container) {
        gutil.log(`Restart container: ${container}`);
        npexecSync(`docker restart ${container}`, {stdio: 'inherit'});
    },
    rm: function ({nameId}={}) {
        return through.obj(function (chunk, enc, callback) {
            nameId = nameId || utils.getIdStringFromChunk(chunk);

            if (nameId) {
                gutil.log(`Deleting container with ids: ${nameId}`);
                npexecSync(`docker rm ${nameId}`, {stdio: 'inherit'});
            } else {
                gutil.log(gutil.colors.magenta('No container to remove'));
            }

            this.push(chunk);
            callback();
        });
    },
    rmi: function ({nameId}={}) {
        return through.obj(function (chunk, enc, callback) {
            nameId = nameId || utils.getIdStringFromChunk(chunk);

            if (nameId) {
                gutil.log(`Deleting container with ids: ${nameId}`);
                npexecSync(`docker rmi ${nameId}`, {stdio: 'inherit'});
            } else {
                gutil.log(gutil.colors.magenta('No image to remove'));
            }

            this.push(chunk);
            callback();
        });
    },
    stop: function ({nameId}={}) {
        return through.obj(function (chunk, enc, callback) {
            nameId = nameId || utils.getIdStringFromChunk(chunk);

            if (nameId) {
                gutil.log(`Stopping container with ids: ${nameId}`);
                npexecSync(`docker stop ${nameId} .`, {stdio: 'inherit'});
            } else {
                gutil.log(gutil.colors.magenta('No container to stop'));
            }

            this.push(chunk);
            callback();
        });
    }
};

module.exports = docker;
